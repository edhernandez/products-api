let express = require('express');
let router = express.Router();
let ProductoController = require('../controllers/producto');

router.get('/', ProductoController.obtenerTodos);
router.post('/', ProductoController.guardar);
router.patch('/:id', ProductoController.actualizar);

module.exports = router;
