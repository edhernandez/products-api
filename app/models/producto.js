let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ProductoSchema = new Schema({
  nombre: {type:'String', required:'true',unique:true, background: false},
  cantidad: {type: 'Number', default:0, min:0}
});

module.exports = mongoose.model('producto', ProductoSchema);
