let ProductModel = require('../models/producto');
let mongoose = require('mongoose');

module.exports = {
    obtenerTodos: (req, res, next) => {
        ProductModel.find({}, function (err, products) {
            res.send(products);
        });
    },
    guardar: (req, res, next) => {
        if (Object.keys(req.body).length !== 0) {
            new ProductModel(req.body).save(function (err, product) {
                if (err && err.code === 11000)
                    res.status(409).send('Ya existe un producto con ese nombre.');
                res.status(201).send(product);
            });
        } else {
            res.status(400).send('No se puede crear un producto sin nombre.');
        }
    },
    actualizar: (req, res, next) => {
        ProductModel.findOne(mongoose.Types.ObjectId(req.params.id), function (err, product) {
            if (product) {
                product.cantidad = product.cantidad + req.body.cantidad;
                product.save(function (err, product) {
                    if (err) {
                        res.status(409).send("La cantidad luego de la operacion es menor a 0.");
                    }
                    res.status(200).send(product);
                });
            } else {
                res.status(404).send("Id: " + req.params.id + " no existe");
            }
        });
    }
}
