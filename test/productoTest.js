process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let ProductModel = require('../app/models/producto');
let chai = require('chai');
let chaiHttp = require('chai-http');

let config = require('config');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

let productoTestCases = require('./productoTestCases');

describe('Products', () => {
    beforeEach((done) => { //Antes de cada test limpio la base
      ProductModel.remove({},function(){
          new ProductModel({nombre:"Manzana", cantidad:10}).save().then(function() {
          done();
        });
      });
    });

    describe('GET /productos', () => {
      it('Debe obtener todos los productos', productoTestCases.getProductos_ConProductosEnLaBD_retornaListaConUnProductoYStatus200);
    });
    describe('POST /productos', () => {
        it('Debe crear un nuevo producto', productoTestCases.postProductos_conProductoValido_retornaProductoYStatus201);
        it('Debe devolver status 400', productoTestCases.postProductos_conBodyVacio_retornaStatus400);
        it('Debe devolver status 409', productoTestCases.postProductos_conProductoExistente_retornaStatus409);
        it('Debe crear un nuevo producto con cantidad 0', productoTestCases.postProductos_sinCantidad_retornaProductoConCantidadCeroYStatus201);
    });
    describe('PATCH /productos', () => {
      it('Debe actualizar una cantidad positiva', productoTestCases.patchProductos_conIdValidoYCantidadPositiva_retornaProductoYStatus200);
      it('Debe devolver status 404', productoTestCases.patchProductos_conIdInvalido_retornaStatus404)
      it('Debe actualizar una cantidad negativa', productoTestCases.patchProductos_conIdValidoYCantidadNegativaValida_retornaProductoYStatus200);
      it('Debe devolver status 409', productoTestCases.patchProductos_conIdValidoYCantidadNegativaInvalida_retornaStatus409);
    });
});
